
# CSD Project

This project and all of it's concepts were built specifically for the Dependable Distributed Systems course, for the faculty: 
**Faculdade de Ciências e Tecnlogias**, Portugal.

## Authors

* **António Barreto** - 45101 (https://bitbucket.org/antoniojsb)
* **Khrystyna Fedyuk** - 45169 (https://bitbucket.org/k_fedyuk)

## Architecture
![Alt text](Architecture.png height="500" "Architecture")

## Project Structure
| Class | File |
| ------ | ------ |
| Wallet Client | [src/rest/client/WalletManager.java](https://bitbucket.org/antoniojsb/csd-project/src/TP2-before-scone/src/rest/client/WalletManager.java) |
| Wallet Server (BFT-Client)| [src/rest/server/WalletServer.java](https://bitbucket.org/antoniojsb/csd-project/src/TP2-before-scone/src/rest/server/WalletServer.java) |
| BFT-Server| [src/rest/server/BFTWalletServer.java](https://bitbucket.org/antoniojsb/csd-project/src/TP2-before-scone/src/rest/server/BFTWalletServer.java) |
| Reply Verification Method| [src/rest/resources/Util.java](https://bitbucket.org/antoniojsb/csd-project/src/TP2-before-scone/src/rest/resources/Util.java#lines-27) |
| Admin Service| [src/adminService](https://bitbucket.org/antoniojsb/csd-project/src/TP2-before-scone/src/adminService/) |
| Secure Service| [src/secureService](https://bitbucket.org/antoniojsb/csd-project/src/TP2-before-scone/src/secureService/) |
| Benchmark| [src/test/WalletTest.java](https://bitbucket.org/antoniojsb/csd-project/src/TP2-before-scone/src/test/WalletTest.java) |
| BFT-SMaRt Library| [src/bftsmart](https://bitbucket.org/antoniojsb/csd-project/src/TP2-before-scone/src/bftsmart/) |
| Key Generation Library| [src/security](https://bitbucket.org/antoniojsb/csd-project/src/TP2-before-scone/src/security/) |
| Homomorphic Encryption Library| [src/hlib/hj](https://bitbucket.org/antoniojsb/csd-project/src/TP2-before-scone/src/hlib/hj/) |