package rest.client;

import org.glassfish.jersey.client.ClientConfig;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

public class WalletClient {
    public static void main(String[] args) {
        Client client = ClientBuilder.newBuilder()
                .hostnameVerifier(new InsecureHostnameVerifier())
                .build();

        URI baseURI = UriBuilder.fromUri("https://0.0.0.0:8080/").build();

        WebTarget target = client.target(baseURI);

        Response response = target.path("wallet/createMoney/" + "client1")
                .request()
                .post( Entity.entity( 40.0 , MediaType.APPLICATION_JSON));

        System.out.println("Crete Money: " + response.getStatus() );

        double balance = target.path("wallet/currentAmount/" + "client1")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get(double.class);

        System.out.println("Get balance: " + balance );

    }

    static public class InsecureHostnameVerifier implements HostnameVerifier {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }
}
