package rest.client;

import io.netty.handler.codec.json.JsonObjectDecoder;
import org.glassfish.jersey.client.ClientConfig;
import org.json.JSONObject;
import rest.server.WalletServer;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import javax.sound.midi.Soundbank;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.Scanner;

public class WalletInteractiveClientIDE {
    public static void main(String[] args) {
        Client client = ClientBuilder.newBuilder()
                .hostnameVerifier(new InsecureHostnameVerifier())
                .build();


        URI baseURI = UriBuilder.fromUri("https://0.0.0.0:8080/").build();

        WebTarget target = client.target(baseURI);

        Scanner scanner = new Scanner(System.in);
        boolean exit = false;
        Response response;
        JSONObject reply;

        while(!exit) {
            System.out.println("\nSelect an option:");
            System.out.println("0 - Terminate this client");
            System.out.println("1 - Create bank account");
            System.out.println("2 - Transfer amount");
            System.out.println("3 - Retrieve current balance");

            System.out.print("Option:");
            int cmd = Integer.parseInt(scanner.nextLine());

            switch (cmd) {
                case 0:
                    exit = true;
                    break;
                case 1:
                    System.out.println("\nCreating bank account");
                    System.out.print("Enter the user: ");
                    String user = scanner.nextLine();
                    System.out.print("Enter the amount: ");
                    double amount = Double.parseDouble(scanner.nextLine());
                    response = target.path("wallet/createMoney/" + user)
                            .request()
                            .accept(MediaType.APPLICATION_JSON)
                            .post(Entity.entity(amount, MediaType.APPLICATION_JSON));
                    System.err.println("Create account: " + response.getStatusInfo());

                    if(response.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL){
                        reply = new JSONObject(response.readEntity(String.class));
                        showResult(reply, response.getStatusInfo().getFamily());
                    }


                    break;
                case 2:
                    System.out.println("\nTransferring amount");
                    System.out.print("Enter the sender user: ");
                    String fromUser = scanner.nextLine();
                    System.out.print("Enter the recipient user: ");
                    String toUser = scanner.nextLine();
                    System.out.print("Enter the amount: ");
                    double transferAmount = Double.parseDouble(scanner.nextLine());
                    response = target.path("wallet/transfer/" + fromUser + "/" + toUser)
                            .request()
                            .accept(MediaType.APPLICATION_JSON)
                            .put(Entity.entity(transferAmount, MediaType.APPLICATION_JSON));

                    System.err.println("Transfer amount: " + response.getStatusInfo());

                    if(response.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL) {
                        reply = new JSONObject(response.readEntity(String.class));

                        showResult(reply, response.getStatusInfo().getFamily());
                    }
                    break;
                case 3:
                    System.out.println("\nRetrieving account balance");
                    System.out.print("Enter the user: ");
                    user = scanner.nextLine();
                    response = target.path("/wallet/currentAmount/" + user)
                            .request()
                            .accept(MediaType.APPLICATION_JSON)
                            .get(Response.class);

                    reply = new JSONObject(response.readEntity(String.class));

                    String userBalance = reply.get("value").toString();

                    System.err.println("Current amount: " + userBalance + "Operation:" + response.getStatusInfo());

                    showResult(reply, response.getStatusInfo().getFamily());

                    break;
                default:
                    break;
            }
        }
    }

    static private void showResult(JSONObject reply, Response.Status.Family status){
        if(status != Response.Status.Family.SERVER_ERROR){
            if(correctResult(reply))
                System.out.println("Correct result!");
            else
                System.out.println("Incorrect result!");
        }
    }

    static private boolean correctResult(JSONObject replies){
        int correctReplies = 0;
        for(String key : replies.keySet()){
            for(String otherKey : replies.keySet()){
                if(!key.equals(otherKey)){
                    if(replies.get(key).equals(replies.get(otherKey)))
                        correctReplies++;
                }
                if(correctReplies >= (WalletServer.NREPLICAS-1)/3 + 1)
                    return true;
            }
        }
        return false;
    }

    static public class InsecureHostnameVerifier implements HostnameVerifier {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }
}
