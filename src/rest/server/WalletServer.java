package rest.server;

import bftsmart.tom.ServiceProxy;
import bftsmart.tom.core.messages.TOMMessage;
import bftsmart.tom.util.Extractor;
import bftsmart.tom.util.KeyLoader;
import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.json.JSONObject;

import javax.net.ssl.SSLContext;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.io.*;
import java.net.InetAddress;
import java.net.URI;
import java.util.Comparator;
import java.util.Scanner;

public class WalletServer {

    public static final int NREPLICAS = 4;

    public static void main(String[] args) throws Exception{
        int port = 8080;
        if (args.length > 0)
            port = Integer.parseInt(args[0]);

        String clientId;

        Scanner console = new Scanner(System.in);
        System.out.print("Type in <server id>: ");
        clientId = console.next();

        URI baseUri = UriBuilder.fromUri("https://0.0.0.0/").port(port).build();

        ResourceConfig config = new ResourceConfig();
        config.register(new service(Integer.parseInt(clientId)));

        JdkHttpServerFactory.createHttpServer(baseUri, config, SSLContext.getDefault());

        System.err.println("SSL REST Wallet Server ready @ " + baseUri + " : host :" + InetAddress.getLocalHost().getHostAddress());
    }

    @Path("/wallet")
    public static class service {

        TOMMessage[] replies = new TOMMessage[NREPLICAS];

        ServiceProxy walletProxy;
        Comparator<byte[]> replyComparator = new Comparator<byte[]>() {
            @Override
            public int compare(byte[] o1, byte[] o2) {
                return 0;
            }
        };
        Extractor replyExtractor = new Extractor() {
            @Override
            public TOMMessage extractResponse(TOMMessage[] tomMessages, int i, int i1) {
                replies = tomMessages;
                return tomMessages[i1];
            }
        };
        KeyLoader loader;

        private service(int id) {
            walletProxy = new ServiceProxy(id, null, replyComparator, replyExtractor, loader);
        }

        private JSONObject extractReplies (){
            JSONObject clientReplies = new JSONObject();
            for (int i = 0; i < replies.length; i++){
                if(replies[i] != null)
                    clientReplies.put("reply"+ i, replies[i].getOperationId() + " " + replies[i].getSequence());
            }
            return clientReplies;
        }

        @GET
        @Path("currentAmount/{id}")
        @Produces(MediaType.APPLICATION_JSON)
        public Response current_amount(@PathParam("id") String id) {
            System.err.printf("retrieving balance from user: %s\n", id);
            try (ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
                 ObjectOutput objOut = new ObjectOutputStream(byteOut)) {

                objOut.writeObject(WalletRequestType.RETRIEVE);
                objOut.writeObject(id);

                objOut.flush();
                byteOut.flush();

                byte[] reply = walletProxy.invokeUnordered(byteOut.toByteArray());

                JSONObject jsonString = extractReplies();

                if (reply.length == 0)
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
                try (ByteArrayInputStream byteIn = new ByteArrayInputStream(reply);
                     ObjectInput objIn = new ObjectInputStream(byteIn)) {

                    jsonString.put("value", objIn.readObject());

                    return Response.status(Response.Status.OK).entity(jsonString.toString()).build();
                }

            } catch (IOException | ClassNotFoundException e) {
                System.err.printf("Exception retrieving balance from user %s: ", id);
                System.out.println(e.getMessage());
            }
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        @POST
        @Path("createMoney/{id}")
        @Consumes(MediaType.APPLICATION_JSON)
        public Response create_money(@PathParam("id") String id, double amount) {
            System.err.printf("create user: %s with <%s> balance\n", id, amount);
            try (ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
                 ObjectOutput objOut = new ObjectOutputStream(byteOut)) {

                objOut.writeObject(WalletRequestType.CREATE);
                objOut.writeObject(id);
                objOut.writeObject(amount);

                objOut.flush();
                byteOut.flush();

                walletProxy.invokeOrdered(byteOut.toByteArray());

                String jsonString = extractReplies().toString();

                return Response.status(Response.Status.OK).entity(jsonString).build();

            } catch (IOException e) {
                System.err.printf("Exception creating money for user %s: ", id);
                System.out.println(e.getMessage());
            }
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        @PUT
        @Path("transfer/{from}/{to}")
        @Consumes(MediaType.APPLICATION_JSON)
        public Response transfer(@PathParam("from") String from, @PathParam("to") String to, double amount) {
            System.err.printf("transfer <%s>$: from %s to %s\n", amount, from, to);
            try (ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
                 ObjectOutput objOut = new ObjectOutputStream(byteOut)) {

                objOut.writeObject(WalletRequestType.TRANSFER);
                objOut.writeObject(from);
                objOut.writeObject(to);
                objOut.writeObject(amount);

                objOut.flush();
                byteOut.flush();

                byte[] reply = walletProxy.invokeOrdered(byteOut.toByteArray());

                String jsonString = extractReplies().toString();

                if (reply.length == 0)
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
                try (ByteArrayInputStream byteIn = new ByteArrayInputStream(reply);
                     ObjectInput objIn = new ObjectInputStream(byteIn)) {
                    if (objIn.readBoolean())
                        return Response.status(Response.Status.OK).entity(jsonString).build();
                    else
                        return Response.status(Response.Status.FORBIDDEN).entity(jsonString).build();

                }
            } catch (IOException e) {
                System.err.printf("Exception transferring amount from %s to %s: ", from, to);
                System.out.println(e.getMessage());
            }
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
