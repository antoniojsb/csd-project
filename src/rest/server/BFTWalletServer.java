package rest.server;

import bftsmart.tom.MessageContext;
import bftsmart.tom.ServiceReplica;
import bftsmart.tom.server.defaultservices.DefaultSingleRecoverable;

import java.io.*;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BFTWalletServer extends DefaultSingleRecoverable {

    //Wallet DB
    private Map<String, Double> db;
    private Logger logger;

    public BFTWalletServer(int id) {
        db = new ConcurrentHashMap<>();
        logger = Logger.getLogger(BFTWalletServer.class.getName());
        new ServiceReplica(id, this, this);
    }

    public static void main(String[] args){
        String clientId;
        if (args.length == 0) {
            Scanner console = new Scanner(System.in);
            System.out.print("Type in <server id>: ");
            clientId = console.next();
            new BFTWalletServer(Integer.parseInt(clientId));
        }
        else
            new BFTWalletServer(Integer.parseInt(args[0]));
    }

    @Override
    public byte[] appExecuteOrdered(byte[] command, MessageContext msgCtx) {
        byte[] reply = null;
        String id = null;
        Double amount = null;
        boolean hasReply = false;

        try (ByteArrayInputStream byteIn = new ByteArrayInputStream(command);
             ObjectInput objIn = new ObjectInputStream(byteIn);
             ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
             ObjectOutput objOut = new ObjectOutputStream(byteOut);){

            WalletRequestType reqType = (WalletRequestType)objIn.readObject();
            switch (reqType) {
                case RETRIEVE:
                    id = (String)objIn.readObject();
                    amount = db.get(id);
                    System.out.println(amount);
                    if (amount != null)
                        objOut.writeObject(amount);
                    else{
                        System.out.println("a mandar 0");
                        objOut.writeObject(0);
                    }

                        hasReply = true;
                    System.err.printf("retrieving balance from user: %s\n", id);
                    break;
                case CREATE:
                    id = (String) objIn.readObject();
                    amount = (Double) objIn.readObject();
                    if (!db.containsKey(id))
                        db.put(id, amount);
                    else
                        db.put(id, db.get(id) + amount);
                    System.err.printf("create user: %s with <%s> balance\n", id, amount);
                    break;

                case TRANSFER:
                    String from = (String) objIn.readObject();
                    String to = (String) objIn.readObject();
                    amount = (Double) objIn.readObject();
                    if (db.containsKey(from) && db.containsKey(to)){
                        if (db.get(from) >= amount){
                            db.put(from, db.get(from) - amount);
                            db.put(to, db.get(to) + amount);
                            System.err.printf("transfer <%s>$: from %s to %s\n", amount, from, to);
                            objOut.writeBoolean(true);
                        }
                    }
                    else
                        objOut.writeBoolean(false);
                    hasReply = true;
                    break;
            }
            if (hasReply) {
                objOut.flush();
                byteOut.flush();
                reply = byteOut.toByteArray();
            } else {
                reply = new byte[0];
            }

        }catch (IOException | ClassNotFoundException e) {
            logger.log(Level.SEVERE, "Occurred during wallet operation execution", e);
        }
        return reply;
    }

    @Override
    public byte[] appExecuteUnordered(byte[] command, MessageContext msgCtx) {
        byte[] reply = null;
        String id = null;
        Double amount = null;
        boolean hasReply = false;

        try (ByteArrayInputStream byteIn = new ByteArrayInputStream(command);
             ObjectInput objIn = new ObjectInputStream(byteIn);
             ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
             ObjectOutput objOut = new ObjectOutputStream(byteOut);){

            WalletRequestType reqType = (WalletRequestType)objIn.readObject();
            switch (reqType) {
                case RETRIEVE:
                    id = (String) objIn.readObject();
                    amount = db.get(id);
                    System.out.println(amount);
                    if (amount != null)
                        objOut.writeObject(amount);
                    else {
                        System.out.println("a mandar 0");
                        objOut.writeObject(0);
                    }

                    hasReply = true;
                    System.err.printf("retrieving balance from user: %s\n", id);
                    break;
            }
            if (hasReply) {
                objOut.flush();
                byteOut.flush();
                reply = byteOut.toByteArray();
            } else {
                reply = new byte[0];
            }

        }catch (IOException | ClassNotFoundException e) {
            logger.log(Level.SEVERE, "Occurred during wallet operation execution", e);
        }
        return reply;
    }

    @Override
    public byte[] getSnapshot() {
        try (ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
             ObjectOutput objOut = new ObjectOutputStream(byteOut)) {
            objOut.writeObject(db);
            return byteOut.toByteArray();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error while taking snapshot", e);
        }
        return new byte[0];
    }

    @SuppressWarnings("unchecked")
    @Override
    public void installSnapshot(byte[] state) {
        try (ByteArrayInputStream byteIn = new ByteArrayInputStream(state);
             ObjectInput objIn = new ObjectInputStream(byteIn)) {
            db = (Map<String, Double>)objIn.readObject();
        } catch (IOException | ClassNotFoundException e) {
            logger.log(Level.SEVERE, "Error while installing snapshot", e);
        }
    }
}
