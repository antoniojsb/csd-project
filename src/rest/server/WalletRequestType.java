package rest.server;

public enum WalletRequestType {
    CREATE,
    TRANSFER,
    RETRIEVE;
}
